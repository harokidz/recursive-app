<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Person extends Model
{
    protected $table = "persons";

    public function parent()
    {
        return $this->belongsTo("App\Models\Person", "parent_id", "id");
    }

    public function getParentName(): string
    {
        if ($this->parent !== NULL) {
            return $this->parent->name;
        }
        return "Tidak Ada";
    }

    public function getSexName()
    {
        return $this->sex === "M" ? "Male" : "Female";
    }

    public static function getGrandChildren($id, $sex = null)
    {
        $query = "select cucu.*
                    from persons as cucu
                             join persons as orangtua on orangtua.id = cucu.parent_id
                        and orangtua.parent_id = $id";
        if ($sex !== null) {
            $query .= " and cucu.sex='$sex'";
        }
        return DB::select($query);
    }

    public static function getCousins($name, $sex = null)
    {
        $query = "select cucu.*
                    from persons as orangtua
                             join (
                        select f.*, o.parent_id as 'grandparent_id'
                        from persons as f
                                 join persons as o on f.parent_id = o.id
                        where f.name = '$name'
                    ) as hani on orangtua.parent_id = hani.grandparent_id
                    join persons as cucu on cucu.parent_id = orangtua.id and cucu.name != hani.name and orangtua.id != hani.parent_id";
        if ($sex !== null) {
            $query .= " and cucu.sex='$sex'";
        }
        return DB::select($query);
    }

    public static function getAunt($name)
    {
        $query = "select orangtua.*
                    from persons as orangtua
                             join (
                        select f.*, o.parent_id as 'grandparent_id'
                        from persons as f
                                 join persons as o on f.parent_id = o.id
                        where f.name = '$name'
                    ) as farah on orangtua.parent_id = farah.grandparent_id
                        and orangtua.sex = 'F'";
        return DB::select($query);
    }

    public static function selectOrangtua($id)
    {
        $query = "SELECT T2.* "
            . "FROM ( SELECT @r AS _id, (SELECT @r :=parent_id FROM persons WHERE id=_id) "
            . "AS parent_id, @l :=@l + 1 AS lvl "
            . "FROM (SELECT @r :=$id, @l :=0) vars, persons m WHERE @r <> 0) T1 "
            . "JOIN persons T2 ON T1._id=T2.id ORDER BY T1.lvl DESC";
        return collect(DB::select($query))->toArray();
    }

    public static function selectAnak($id)
    {
        $query = "SELECT  id, "
            . "name, sex, "
            . "parent_id "
            . "FROM    (SELECT * FROM persons "
            . "ORDER BY parent_id, id) tablename, "
            . "(SELECT @pv := $id) initialisation "
            . "WHERE   FIND_IN_SET(parent_id, @pv) > 0 "
            . "AND     @pv := CONCAT(@pv, ',', id)";
        return collect(DB::select($query))->toArray();
    }
}
