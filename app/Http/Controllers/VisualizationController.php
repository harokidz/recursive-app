<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;

class VisualizationController extends Controller
{
    public function index()
    {
        $resultOrangTua = Person::selectOrangtua(1);
        $resultAnak = Person::selectAnak(1);
        $result = array_merge($resultOrangTua, $resultAnak);
        $result = json_decode(json_encode($result), true);
        $result = $this->buildTree(1, $result);
        $data['persons'] = json_encode($result[0]);
        return view('viz', $data);
    }

    private function buildTree($id, array $elements, $nomor = -1, $parentId = 0)
    {

        $branch = array();
        $nomor++;
        foreach ($elements as $element) {
//                if ($element['enabled'] != "1") {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($id, $elements, $nomor, $element['id']);
                $data = array();
                $data["text"] = array(
                    "name" => $element["name"],
                    "title" => "Generasi " . $nomor
                );
//                $data["image"] = "images/no-image.png";
                $data["HTMLclass"] = "person bg-primary text-white";
                if($element['sex'] === 'F'){
                    $data["HTMLclass"] = "person bg-danger text-white";
                }
//                if ($element['id'] == $id) {
//                    $data["HTMLclass"] = "person bg-primary";
//                }
                $data["HTMLid"] = $element["id"];
                if ($children) {
                    $data['children'] = $children;
                }
                $branch[] = $data;
            }
//                }
        }
        return $branch;

    }
}
