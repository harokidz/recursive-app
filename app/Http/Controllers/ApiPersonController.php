<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;

class ApiPersonController extends Controller
{
    public function getAll()
    {
        $persons = Person::all();
        return response()->json($persons);
    }

    public function getById($id)
    {
        $person = Person::find($id);
        return response()->json($person);
    }

    public function store()
    {
        $name = \request('name');
        $sex = \request('sex');
        $parentId = \request('parent_id');
        $person = ["name" => $name, "sex" => $sex, "parent_id" => $parentId];
        Person::insert($person);
        return response()->json([], 201);
    }

    public function getGrandChildren($id)
    {
        $sex= \request('sex');
        $grandChildren = Person::getGrandChildren($id,$sex);
        return response()->json($grandChildren);
    }

    public function getCousins($name)
    {
        $sex= \request('sex');
        $counsins = Person::getCousins($name,$sex);
        return response()->json($counsins);
    }

    public function getAunt($name)
    {
        $autns = Person::getAunt($name);
        return response()->json($autns);
    }
}
