<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    public function index()
    {
        $persons = Person::with('parent')->get();
        return view('list', compact('persons'));
    }

    public function store()
    {
        $person = \request("person");
        Person::insert($person);
        return redirect(url('/'));
    }

    public function update()
    {
        $id = \request('person_id');
        $person = \request("person");
        Person::where(["id"=>$id])->update($person);
        return redirect(url('/'));
    }

    public function delete()
    {
        $id = \request('id');
        Person::where(["id"=>$id])->delete();
        return redirect(url('/'));
    }


}
