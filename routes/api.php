<?php

use App\Http\Controllers\ApiPersonController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/persons',[ApiPersonController::class,'getAll']);
Route::get('/persons/{id}',[ApiPersonController::class,'getById']);
Route::get('/persons/{id}/grand-children',[ApiPersonController::class,'getGrandChildren']);
Route::get('/persons/{id}/cousins',[ApiPersonController::class,'getCousins']);
Route::get('/persons/{id}/aunt',[ApiPersonController::class,'getAunt']);
Route::post('/persons',[ApiPersonController::class,'store']);
