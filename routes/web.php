<?php

use App\Http\Controllers\PersonController;
use App\Http\Controllers\VisualizationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PersonController::class, 'index']);
Route::get('/viz', [VisualizationController::class, 'index']);
Route::post('/person/store', [PersonController::class, 'store']);
Route::post('/person/update', [PersonController::class, 'update']);
Route::post('/person/delete', [PersonController::class, 'delete']);
