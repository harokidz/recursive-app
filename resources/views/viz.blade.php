<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title> Chart example 1 </title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('assets/treant/Treant.css')}}">
    <!--		<link rel="stylesheet" href="simple-scrollbar.css">-->
    <style>
        body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, fieldset, input, textarea, p, blockquote, th, td {
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        fieldset, img {
            border: 0;
        }

        address, caption, cite, code, dfn, em, strong, th, var {
            font-style: normal;
            font-weight: normal;
        }

        caption, th {
            text-align: left;
        }

        h1, h2, h3, h4, h5, h6 {
            font-size: 100%;
            font-weight: normal;
        }

        q:before, q:after {
            content: '';
        }

        abbr, acronym {
            border: 0;
        }

        body {
            background: #fff;
        }

        /* optional Container STYLES */
        .chart {
            margin: 5px;
        }

        .Treant > .node {
        }

        .Treant > p {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-weight: bold;
            font-size: 12px;
        }

        .node-name {
            font-weight: bold;
        }

        .nodeExample1 {
            padding: 2px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            background-color: #ffffff;
            border: 1px solid #000;
            width: 200px;
            font-family: Tahoma;
            font-size: 12px;
            cursor: pointer;
        }

        .nodeExample1 img {
            margin-right: 10px;
            width: 35px;
        }

        .active {
            background: #5dacff;
        }
    </style>
    <style>
        /*.chart {*/
        /*	height: 90% !important;*/
        /*	margin: 5px;*/
        /*	width: 90% !important;*/
        /*	margin: 5px auto;*/
        /*	border: 3px solid #DDD;*/
        /*	border-radius: 3px;*/
        /*}*/
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Submission</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link" href="{{url('/')}}">Home</a>
                        <a class="nav-link" href="{{url('/viz')}}">Visualization</a>
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Data Person</h4>
                </div>
                <div class="card-body">
                    <div class="chart" id="OrganiseChart1"> --@--</div>
                </div>
                <div class="card-footer"></div>
            </div>

        </div>
    </div>
</div>
<script src="{{asset('assets/treant/vendor/raphael.js')}}"></script>
<script src="{{asset('assets/treant/Treant.js')}}"></script>

<script src="{{asset('assets/treant/vendor/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="{{asset('assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
<script>
    let data = JSON.parse('<?= $persons ?>');
    console.log(data);
    var config = {
        container: "#OrganiseChart1",
        rootOrientation: 'NORTH', // NORTH || EAST || WEST || SOUTH
        scrollbar: "fancy",
        // levelSeparation: 30,
        siblingSeparation: 20,
        subTeeSeparation: 60,

        connectors: {
            type: 'step'
        },
        node: {
            HTMLclass: 'nodeExample1'
        }
    };
    ALTERNATIVE = [
        config,
        data
    ];
    new Treant(ALTERNATIVE);
    $(function () {
        {{--$(".person").click(function () {--}}
        {{--    let id = $(this).attr("id");--}}
        {{--    window.location.replace("<?=site_url("dashboard/index/")?>"+id);--}}
        {{--});--}}
    });
</script>
</body>
</html>
