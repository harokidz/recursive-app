<!doctype html>
<html lang="en">
<head>
    <title>Javan</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Submission</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link" href="{{url('/')}}">Home</a>
                        <a class="nav-link" href="{{url('/viz')}}">Visualization</a>
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Data Person</h4>
                </div>
                <div class="card-body">
                    <button type="button" id="btn-add" class="btn btn-primary mb-1">Tambah</button>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Sex</th>
                                <th>Parent</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1)
                            @foreach($persons as $person)

                                <tr>
                                    <td scope="row">{{$i++}}</td>
                                    <td>{{$person->name}}</td>
                                    <td>{{$person->getSexName()}}</td>
                                    <td>{{$person->getParentName()}}</td>
                                    <td>
                                        <button
                                            data-id="{{$person->id}}"
                                            data-name="{{$person->name}}"
                                            data-sex="{{$person->sex}}"
                                            data-parent-id="{{$person->parent_id}}"
                                            class="btn btn-warning btn-sm btn-update">Edit
                                        </button>
                                        <button
                                            data-id="{{$person->id}}"
                                            data-name="{{$person->name}}"
                                            data-sex="{{$person->sex}}"
                                            data-parent-id="{{$person->parent_id}}"
                                            class="btn btn-danger btn-sm btn-delete">Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer"></div>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<form action="{{url('person/store')}}" method="post">
    @csrf
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Person</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input required type="text" name="person[name]" id="" class="form-control" placeholder=""
                               aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">Sex</label>
                        <select required name="person[sex]" id="" class="form-control">
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Parent</label>
                        <select required name="person[parent_id]" id="" class="form-control">
                            @foreach($persons as $person)
                                <option value="{{$person->id}}">{{$person->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- Modal -->
<form action="{{url('person/update')}}" method="post">
    @csrf
    <input type="hidden" name="person_id" id="u-id">
    <div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Person</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input required type="text" name="person[name]" id="u-name" class="form-control" placeholder=""
                               aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">Sex</label>
                        <select required name="person[sex]" id="u-sex" class="form-control">
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Parent</label>
                        <select required name="person[parent_id]" id="u-parent" class="form-control">
                            @foreach($persons as $person)
                                <option value="{{$person->id}}">{{$person->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script>
    $(function () {
        $('#btn-add').click(function () {
            $("#modal-add").modal("show");
        });
        $(".btn-update").click(function () {
            let id = $(this).data("id");
            let sex = $(this).data("sex");
            let name = $(this).data("name");
            let parentId = $(this).data("parent-id");
            $("#u-id").val(id);
            $("#u-sex").val(sex).trigger('change');
            $("#u-name").val(name);
            $("#u-parent").val(parentId).trigger('change');

            $("#modal-update").modal("show");
        });
        $(".btn-delete").click(function () {
            let id = $(this).data("id");
            let sex = $(this).data("sex");
            let name = $(this).data("name");
            let parentId = $(this).data("parent-id");
            $.confirm({
                title: 'Konfirmasi!',
                content: 'Anda yakin hapus data dengan detail : <br>' + name + ' (' + sex + ') ?',
                buttons: {
                    cancel: function () {
                    },
                    hapus: {
                        text: 'Hapus',
                        btnClass: 'btn-red',
                        action: function () {
                            var newForm = $('<form>', {
                                'action': '{{url('person/delete')}}',
                                'method': 'post'
                            }).append($('<input>', {
                                'name': '_token',
                                'value': '{{csrf_token()}}',
                                'type': 'hidden'
                            })).append($('<input>', {
                                'name': 'id',
                                'value': id,
                                'type': 'hidden'
                            }));
                            newForm.appendTo("body").submit();
                        }
                    }
                }
            });
        });
    });
</script>
</body>
</html>
