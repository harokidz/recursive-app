<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('persons')->insert([
            ["id" => 1, "name" => "Budi", "sex" => "M", "parent_id" => 0],
            ["id" => 2, "name" => "Dedi", "sex" => "M", "parent_id" => 1],
            ["id" => 3, "name" => "Dodi", "sex" => "M", "parent_id" => 1],
            ["id" => 4, "name" => "Dede", "sex" => "M", "parent_id" => 1],
            ["id" => 5, "name" => "Dewi", "sex" => "F", "parent_id" => 1],
            ["id" => 6, "name" => "Feri", "sex" => "M", "parent_id" => 2],
            ["id" => 7, "name" => "Farah", "sex" => "F", "parent_id" => 2],
            ["id" => 8, "name" => "Gugus", "sex" => "M", "parent_id" => 3],
            ["id" => 9, "name" => "Gandi", "sex" => "M", "parent_id" => 3],
            ["id" => 10, "name" => "Hani", "sex" => "F", "parent_id" => 4],
            ["id" => 11, "name" => "Hana", "sex" => "F", "parent_id" => 4]
        ]);
    }
}
