#Buat query untuk mendapatkan semua anak Budi
select *
from persons
where parent_id = 1;

#4. Buat query untuk mendapatkan cucu dari budi
select cucu.*
from persons as cucu
         join persons as orangtua on orangtua.id = cucu.parent_id
    and orangtua.parent_id = 1;

#5. Buat query untuk mendapatkan cucu perempuan dari budi
select cucu.*
from persons as cucu
         join persons as orangtua on orangtua.id = cucu.parent_id
    and orangtua.parent_id = 1 and cucu.sex = 'F';

#6. Buat query untuk mendapatkan bibi dari Farah
select orangtua.*
from persons as orangtua
         join (
    select f.*, o.parent_id as 'grandparent_id'
    from persons as f
             join persons as o on f.parent_id = o.id
    where f.name = 'Farah'
) as farah on orangtua.parent_id = farah.grandparent_id
    and orangtua.sex = 'F';

#7. Buat query untuk mendapatkan sepupu laki-laki dari Hani
select cucu.*
from persons as orangtua
         join (
    select f.*, o.parent_id as 'grandparent_id'
    from persons as f
             join persons as o on f.parent_id = o.id
    where f.name = 'Hani'
) as hani on orangtua.parent_id = hani.grandparent_id
         join persons as cucu on cucu.parent_id = orangtua.id
    and cucu.name != hani.name
    and cucu.sex = 'M'
    and orangtua.id != hani.parent_id;
#8. Buat aplikasi CRUD untuk silsilah keluarga
#9. Buat visualisasi tree untuk data yang diinput di nomer 1
#10. Rancang dan buat API yang bisa dibuat untuk kasus di atas
